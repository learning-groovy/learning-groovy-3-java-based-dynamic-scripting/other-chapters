package org.elu.learn.groovy

import groovy.time.TimeCategory

public class Person {
    def eatDonuts() { println("yummy") }
}

public class RoboCop {
    @Delegate
    final Person person

    public RoboCop(Person person) { this.person = person }

    public RoboCop() { this.person = new Person() }

    def crushCars() {
        println("smash")
    }
}

class Chapter06 {
    static def totalPricesLessThan10(prices) {
        int total = 0
        for (int price : prices)
            if (price < 10) total += price
        total
    }

    static def totalPricesMoreThan10(prices) {
        int total = 0
        for (int price : prices)
            if (price > 10) total += price
        total
    }

    static def totalPricesAll(prices) {
        int total = 0
        for (int price : prices)
            total += price
        total
    }
    // replace 3 methods above with single method
    static def totalPrices(prices, selector) {
        prices.findAll(selector).sum()
    }

    // Missing methods
    def methodMissing(String name, args) {
        println "in methodMissing"
        def impl = { println name }
        // to improve efficiency, you intercept, cache, and invoke the called method
        getMetaClass()."$name" = impl
        impl()
    }

    static void main(String[] args) {
        println 'Groovy Design Patterns'
        println()

        // Strategy Pattern
        def prices = [1, 5, 10, 15, 20]
        println totalPricesMoreThan10(prices)
        println totalPricesLessThan10(prices)
        println totalPricesAll(prices)

        println totalPrices(prices, { it > 10 })
        println totalPrices(prices, { it < 10 })
        println totalPrices(prices, { true })
        println()

        // Meta-Programming
        // Meta-Class
        String.metaClass.words = { -> split(/ +/) }
        def text = "the lazy brown fox"
        println text.words() // Result: ['the', 'lazy', 'brown', 'fox']

/*
        HttpSession.metaClass.getAt = { key -> getAttribute(key) }
        HttpSession.metaClass.putAt = {
            key, value -> setAttribute(key, value)
        }
        def session = request.session
        session['my_id'] = '123' // calls putAt('my_id', '123')
        def id = session['my_id'] // calls getAt('my_id')
*/
        println()

        // Categories
        def now = new Date()
        println now
        use(TimeCategory) {
            def nextWeekPlusTenHours = now + 1.week + 10.hours
            println nextWeekPlusTenHours
        }
        println()

        // Missing Methods
        def ch = new Chapter06()
        ch.wow()
        ch.thisWorks()
        ch.wow()
        println()

        // Delegation
        def person = new RoboCop()
        person.eatDonuts()
        person.crushCars()
        println()
    }
}
