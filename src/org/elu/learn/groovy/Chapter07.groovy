package org.elu.learn.groovy

class SMS {
    String from, to, body;

    def from(String fromNumber) {
        from = fromNumber
    }

    def to(String toNumber) {
        to = toNumber
    }

    def body(String body) {
        this.body = body
    }

    def send() {
        // send the text.
        println "From: $from, to: $to, body: $body"
    }

    // In Groovy can add the following static method to the SMS class for DSL-like usage
    def static send(@DelegatesTo(SMS) Closure block) {
        SMS m = new SMS()
        block.delegate = m
        block()
        m.send()
    }
}

// this supports the command chain syntax
class SMS2 {
    String from, to, body;

    def from(String fromNumber) {
        this.from = fromNumber; return this
    }

    def to(String toNumber) {
        this.to = toNumber; return this
    }

    def body(String body) {
        this.body = body; return this
    }

    def send() {
        // send the text.
        println "From: $from, to: $to, body: $body"
    }

    def static send(@DelegatesTo(SMS) Closure block) {
        SMS m = new SMS()
        block.delegate = m
        block()
        m.send()
    }
}

// Overriding Operators
class Logic {
    boolean value

    Logic(v) { this.value = v }

    def and(Logic other) {
        this.value && other.value
    }

    def or(Logic other) {
        this.value || other.value
    }
}

// leftShift and minus operators
class Wizards {
    def list = []

    def leftShift(person) { list.add person }

    def minus(person) { list.remove person }

    String toString() { "Wizards: $list" }
}

// Missing Methods and Properties
// Represents a chemical Element
class Element {
    String symbol

    Element(s) { symbol = s }

    double getWeight() { symbol == 'H' ? 1.00794 : 15.9994 }

    String toString() { symbol }
}
// Represents a chemical Compound
class Compound {
    final Map elements = [:]

    Compound(String str) {
        def matcher = str =~ /([A-Z][a-z]*)([0-9]+)?/
        while (matcher.find()) add(
            new Element(matcher.group(1)),
            (matcher.group(2) ?: 1) as Integer)
    }

    void add(Element e, int num) {
        if (elements[e]) elements[e] += num
        else elements[e] = num
    }

    double getWeight() {
        elements.keySet().inject(0d) { sum, key ->
            sum + (key.weight * elements[key])
        }
    }

    String toString() { "$elements" }
}

class Chemistry {
    static void exec(Closure block) {
        block.delegate = new Chemistry()
        block()
    }

    def propertyMissing(String name) {
        def comp = new Compound(name)
        (comp.elements.size() == 1 && comp.elements.values()[0] == 1) ?
            comp.elements.keySet()[0] : comp
    }
}

class Chapter07 {
    static void main(String[] args) {
        println 'DSLs'
        println()

        // Closure with Delegate
        // In Java need to use this the following way:
        SMS m = new SMS();
        m.from("555-432-1234");
        m.to("555-678-4321");
        m.body("Hey there!");
        m.send();

        // In Groovy can use DSL style of static method call
        SMS.send {
            from '555-432-1234'
            to '555-678-4321'
            body 'Hey there!'
        }
        println()

        // Command Chains
        SMS2.send {
            from '555-432-1234'
            to '555-678-4321'
            body 'Hey there!'
        }
        println()

        // Overriding Operators
        def pale = new Logic(true)
        def old = new Logic(false)

        println "groovy truth: ${pale && old}" //true
        println "using and: ${pale & old}" // false
        println "using or: ${pale | old}" // true
        println()

        def wiz = new Wizards()
        wiz << 'Gandolf'
        println wiz // Wizards: [Gandolf]
        wiz << 'Harry'
        println wiz // Wizards: [Gandolf, Harry]
        wiz - 'Harry'
        println wiz // Wizards: [Gandolf]
        println()

        // Missing Methods and Properties
        def c = new Chemistry()
        def water = c.H2O
        println water // [H: 2, O: 1]
        println water.weight // 18.01528
        println()

        Chemistry.exec {
            def w = H2O
            println w
            println w.weight
        }
        println()
    }
}
