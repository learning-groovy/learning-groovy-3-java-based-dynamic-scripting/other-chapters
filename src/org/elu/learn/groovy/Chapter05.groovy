package org.elu.learn.groovy

import groovy.transform.Canonical

@Canonical
class Dragon2 {
    def name
}

class Pie {
    def bake() { this }

    def make() { this }

    def eat() { this }
}

class Chapter05 {
    static def fly(String text = 'flying') { println text }

    static def doStuff(int ... args) { println args }

    static void main(String[] args) {
        println 'Coming from Java'
        println()

        // Default Method Values
        fly()
        fly 'Groovy'
        println()

        // Equals, HashCode, and More
        println new Dragon2('Smaug')
        assert new Dragon2('') == new Dragon2('')
        println new Dragon2('') == new Dragon2('')
        println()

        // Regex Pattern Matching
        def email = 'mailto:adam@email.com'
        def mr = email =~ /[\w.]+@[\w.]+/
        if (mr.find()) println mr.group()
        println()

        // Missing Java Syntax
        String[] array = ['foo', 'bar'].toArray()
        for (String item : array) println(item)
        println("====")
        for (item in array) println(item)
        println("====")
        array.each { item -> println(item) }
        println("====")
        println()

        // Optional Semicolon
        def pie = new Pie().
            make().
            bake().
            eat()

        // Optional Parenthesis Sometimes
        // call a method named "doStuff" with parameter 1
        doStuff 1
        // call "doStuff" with three parameters :
        doStuff 1, 2, 3
        // call "each" on "list" with one Closure:
        def list = [1, 2, 3]
        list.each { item -> doStuff(item) }
        println()

        // Where Are Generics?
        // Groovy supports the syntax of generics but does not enforce them by default
        List<Integer> nums = [1, 2, 3.1415, 'pie']
        println nums
        // Groovy will enforce generics if you add the @CompileStatic or @TypeChecked annotation
        println()

        // Groovy Numbers
        def pieM = 3.141592
        println pieM
        println pieM.getClass()
        pieM = 3.141592d
        println pieM
        println pieM.getClass()
        println()

        // Boolean-Resolution
        if ("foo") println("true")
        if (!"") println("true")
        if (42) println("true")
        if (!0) println("true")
        println()

        // Map Syntax
        def foo = 'key'
        def bar = 2
        def map = [(foo): bar, foobar: 5]
        println map
        println map.key
        println map.foo
        println map.foobar
        println map.keySet()
        println()
    }
}
