package org.elu.learn.groovy

import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.transform.ToString

@ToString
class Dragon {
    String name
}

static void main(String[] args) {
    println 'GDK'
    println()

    // Spread
    def dragons = [new Dragon(name: 'Eka'), new Dragon(name: 'Toka')]
    dragons*.name.each { println it }
    // above line is a shorter form for the following:
    dragons.collect { d -> d.name }.each { println it }
    println()

    // GPath
    def listOfMaps = [['a': 11, 'b': 12], ['a': 21, 'b': 22]]
    assert listOfMaps.a == [11, 21] //GPath notation
    assert listOfMaps*.a == [11, 21] //spread dot notation
    println listOfMaps.a

    listOfMaps = [['a': 11, 'b': 12], ['a': 21, 'b': 22], null]
    assert listOfMaps*.a == [11, 21, null] // caters  for  null  values
    assert listOfMaps*.a == listOfMaps.collect { it?.a } //equivalent  notation
    println listOfMaps*.a
    // But this will only collect non-null values
    assert listOfMaps.a == [11, 21]
    println listOfMaps.a
    println()

    // IO
    // Files
    new File('books.txt').text = 'Modern Java'
    println new File('books.txt').text

    byte[] data = new File('books.txt').bytes
    new File('out').bytes = data
    println new File('out').bytes

    // can use an InputStream or reader or the corresponding OutputStream or writer for output
//    new File('dragons.txt').withOutputStream { out -> }
    new File('dragons.txt').withWriter { w ->
        dragons.each { w.writeLine it.toString() }
    }
//    new File('dragons.txt').withInputStream { in -> }
    new File('dragons.txt').withReader { r ->
        r.readLines().each { println it }
    }

    // can use the eachLine method to read each line of a file
    new File('dragons.txt').eachLine { line ->
        println "$line"
    }
    // OR
    new File('dragons.txt').eachLine { line, num ->
        println "Line $num: $line"
    }
    println()

    // URLs
    URL url = new URL('http://leanpub.com')
    InputStream input = (InputStream) url.getContent()
    ByteArrayOutputStream out = new ByteArrayOutputStream()
    int n = 0;
    byte[] arr = new byte[1024];
    while (-1 != (n = input.read(arr))) {
        out.write(arr, 0, n)
    }
    println arr
    println()
    // in Groovy this also can be reduced to one line
    println 'http://leanpub.com'.toURL().text
    println()

    // Ranges
    (1..4).each { print it } //1234
    println()
    for (i in 1..4) print i //1234
    println()

    def text = 'learning groovy'
    println text[0..4] //learn
    println text[0..4, 8..-1] //learn groovy
    (0..<5).each { print it } //1234
    println()
    (0<..<5).each { print it } //1234
    println()
    println()

    // Utilities
    // ConfigSlurper
    def config = new ConfigSlurper().parse('''
        app.date = new Date()
        app.age  = 42
        app {
            name = "Test${42}"
        }
    ''')

    def properties = config.toProperties()

    assert properties."app.date" instanceof String
    assert properties."app.age" == '42'
    assert properties."app.name" == 'Test42'
    println properties
    println()

    // JsonBuilder and JsonSlurper
    def builder = new JsonBuilder()
    builder.person {
        name 'Adam'
        age 37
        conferences 'Gr8Conf', 'ÜberConf'
    }
    println builder

    def slurper = new JsonSlurper()
    def result = slurper.parseText(builder.toString())
    assert result.person.name == "Adam"
    assert result.person.age == 37
    assert result.person.conferences.size() == 2
    assert result.person.conferences[0] == "Gr8Conf"
    println result
    println()

    // Expando
    def expando = new Expando()
    expando.name = 'Draco' // field value
    expando.say = { String s -> "${name} says $s" } //method
    println expando.say('hello') // Output: Draco says hello
    println()

    // ObservableList/Map/Set
    def list = new ObservableList()
    def printer = { e -> println e.class }
    list.addPropertyChangeListener(printer)
    list.add 'Harry Potter'
    list.add 'Hermione Granger'
    list.remove(0)
    println list
    println()
}
