package org.elu.learn.groovy

trait Animal {
    int hunger = 100

    def eat() { println "eating"; hunger -= 1 }

    abstract int getNumberOfLegs()
}

class Rocket {
    String name

    def launch() { println(name + " Take off!") }
}

trait Shuttle {
    boolean canFly() { true }

    abstract int getCargoBaySize()
}

class MoonShuttle extends Rocket
    implements MoonLander, Shuttle {
    int getCargoBaySize() { 100 }
}

trait MoonLander {
    def land() { println("${getName()} Landing!") }

    abstract String getName()
}

class Apollo extends Rocket implements MoonLander {
}

class Chapter08 {
    static void main(String[] args) {
        println 'Traits'
        println()

        def apollo = new Apollo(name: "Apollo 12")
        apollo.launch()
        apollo.land()
        println()

        MoonShuttle m = new MoonShuttle(name: 'Taxi')
        println "${m.name} can fly? ${m.canFly()}"
        println "cargo bay: ${m.getCargoBaySize()}"
        m.launch()
        m.land()
        println()
    }
}
