package org.elu.learn.groovy

import groovy.transform.Canonical
import groovy.transform.Immutable
import groovy.transform.TailRecursive

class Person09 {
    String name
    int age

    String toString() { name }
}

// Immutability
@Immutable
class Dragon09 {
    String name
    int scales
}

@Canonical class City {int population}

class Chapter09 {
    static def find(list, tester) {
        for (item in list)
            if (tester(item)) return item
    }

    // Method Handles
    static def breathFire(name) { println "Burninating $name!" }

    @TailRecursive
    static long totalPopulation(list, total = 0) {
        if (list.size() == 0)
            total
        else
            totalPopulation(list.tail(), total + list.first().population)
    }

    static void main(String[] args) {
        println 'Functional Programming'
        println()

        // Functions and Closures
        def closr = { x -> x + 1 }
        println(closr(2))
        // one argument closure can be declared like following
        def closr2 = { it + 1 }
        println(closr2(2))
        println()

        println find([1, 2]) { it > 1 }
        println()

        // Map/Filter/And So On
        def persons = [
            new Person09(name: 'Bob', age: 20),
            new Person09(name: 'Tom', age: 15)
        ]
        def names = persons.collect { person -> person.name }
        println names
        def adults = persons.findAll { person -> person.age >= 18 }
        println adults
        def totalAge = persons.inject(0) { total, p -> return total + p.age } //Result: 35
        println totalAge
        println()

        def a = [3, 2, 1]
        def firstTwo = a[0..1] // Result: [3,2]
        println firstTwo
        a = [1, 2, 3]
        def b = [4, 5]
        print a + b // Result: [1, 2, 3, 4, 5]
        println()
        println()

        // Immutability
        def smaug = new Dragon09('Smaug', 499)
        println smaug
        println()

        // Groovy Fluent GDK
        def list = ['foo', 'bar']
        def newList = []
        list.collect(newList) { it.substring(1) }
        println newList //  [oo, ar]

        def dragons = [new Dragon09('Smaug', 499), new Dragon09('Norbert', 488)]
        String longestName = dragons.
            findAll { it.name != null }.
            collect { it.name }.
            inject("") { n1, n2 -> n1.length() > n2.length() ? n1 : n2 }
        println longestName
        println dragons
            .collect { it.scales }
            .inject(0) { s1, s2 -> s1 > s2 ? s1 : s2 }
        println()

        // Groovy Curry
        def concat = { x, y -> return x + y }
        def burn = concat.curry('burn')
        def inate = concat.curry('inate')
        println burn(' wood')

        def composition = { f, g, x -> return f(g(x)) }
        def burninate = composition.curry(burn, inate)
        def trogdor = burninate(' all the people')
        println "Trogdor: ${trogdor}"
        println()

        // Method Handles
        ['the country side', 'all the people'].each(this.&breathFire)
        println()

        // Tail Recursion
        def cities = (10..1000).collect{new City(it)}
        println totalPopulation(cities)
        println()
    }
}
