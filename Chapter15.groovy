@Grapes([
    @Grab('io.ratpack:ratpack-groovy:2.0.0-rc-1'),
    @Grab('org.slf4j:slf4j-simple:1.7.36')
])
import static ratpack.groovy.Groovy.ratpack

ratpack {
    handlers {
        get {
            render "Hello World!"
        }
    }
}

// run with following command: groovy Chapter15.groovy
// test for example with curl: curl http://localhost:5050
